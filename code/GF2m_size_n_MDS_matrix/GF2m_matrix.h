#ifndef GF2M_MATRIX_H_INCLUDED
#define GF2M_MATRIX_H_INCLUDED

#include <iostream>
#include "predefine.h"
#include "GF2m_element.h"

using namespace std;

class GF2mMatrix
{
public:

    CUDA_HEAD GF2mMatrix(){}
    CUDA_HEAD GF2mMatrix(const GF2mMatrix& m);
    CUDA_HEAD ~GF2mMatrix(){}

    CUDA_HEAD void setValue(int row, int col, const GF2mElement& elem);
    void random();
    /*
    * Note: This function doesn't compute the whole matrix's determinant.
    * It's only compute first size N * N of GF2mMatrix
    * Write codes like this is because when calculates determinant, we will change value of GF2mMatrix
    */
    CUDA_HEAD GF2mElement determinantSizeN(const int N);
    CUDA_HEAD GF2mElement determinantAndInverseSizeN(const int N, GF2mMatrix& inverse);

    /*
    * We choose rows and cols, then compute the determinant of sub-matrix
    */
    CUDA_HEAD GF2mElement determinantRowCol(const int rows[], const int cols[], const int N) const;
    CUDA_HEAD GF2mElement determinantRowColAndInverse(const int rows[], const int cols[], GF2mMatrix& inverse, const int N) const;
    CUDA_HEAD GF2mMatrix operator*(const GF2mMatrix& m) const;
    CUDA_HEAD GF2mMatrix& operator=(const GF2mMatrix& m);
    CUDA_HEAD bool operator==(const GF2mMatrix& m) const;
    friend ostream& operator<<(ostream& ostr, const GF2mMatrix& m);
    friend istream& operator>>(istream& istr, GF2mMatrix& m);

    GF2mElement matrix[MATRIX_N][MATRIX_N];
private:

    CUDA_HEAD void swapRowSizeN(const int row1, const int row2, const int begin, const int end);
};

void testDeterminant();
void testInverseMatrix();
#endif // GF2M_MATRIX_H_INCLUDED
