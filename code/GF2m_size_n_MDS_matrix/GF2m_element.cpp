#include <string.h>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>

#include "predefine.h"
#include "GF2m_element.h"


using namespace std;

CUDA_HEAD GF2mElement::GF2mElement()
{
    //srand(time(0));
    #ifdef USE_GPU
    Px = findPx();
    #endif
}

CUDA_HEAD GF2mElement::GF2mElement(const BinaryCode& obj)
{
    biCode = obj;
    #ifdef USE_GPU
    Px = findPx();
    #endif
}

CUDA_HEAD GF2mElement::GF2mElement(const GF2mElement& elem)
{
    biCode = elem.biCode;
    #ifdef USE_GPU
    Px = findPx();
    #endif
}

CUDA_HEAD void GF2mElement::setZero()
{
    biCode.setZero();
}

CUDA_HEAD void GF2mElement::setZero(int pos)
{
    biCode.setZero(pos);
}

CUDA_HEAD void GF2mElement::setOne(int pos)
{
    biCode.setOne(pos);
}

void GF2mElement::random()
{
    int value = 0;
    if(prime_power == 32)
    {
        value = rand();
    }
    else
    {
        value = rand() % (1 << prime_power);
    }
    for(int i = 0; i < value; ++i)
    {
        ++(*this);
    }
}

CUDA_HEAD GF2mElement GF2mElement::operator^(const GF2mElement& elem) const
{
    GF2mElement answer(elem);
    answer.biCode = biCode ^ elem.biCode;
    return answer;
}

CUDA_HEAD GF2mElement GF2mElement::operator*(const GF2mElement& elem) const
{
    GF2mElement answer(elem);
    answer.biCode = ((biCode * elem.biCode) % Px);
    return answer;
}

CUDA_HEAD GF2mElement GF2mElement::operator!() const
{
    BinaryCode t, r(Px), newt, newr(biCode), zero;
    newt.setOne(0);

    BinaryCode quotient, tmpr, tmpt;
    while( newr != zero)
    {
        quotient = (r / newr);

        tmpr = newr;
        newr = (r ^ (quotient * newr));
        r = tmpr;

        tmpt = newt;
        newt = (t ^ (quotient * newt));
        t = tmpt;
    }
    GF2mElement answer;
    answer.biCode = t;
    return answer;
}

CUDA_HEAD bool GF2mElement::operator==(const GF2mElement& elem) const
{
    return biCode == elem.biCode;
}

CUDA_HEAD bool GF2mElement::operator!=(const GF2mElement& elem) const
{
    return ! ((*this) == elem);
}

CUDA_HEAD bool GF2mElement::operator<(const GF2mElement& elem) const
{
    return biCode < elem.biCode;
}

CUDA_HEAD GF2mElement& GF2mElement::operator=(const GF2mElement& elem)
{
    biCode = elem.biCode;
    return *this;
}

CUDA_HEAD GF2mElement& GF2mElement::operator++()
{
    BinaryCode limit;
    limit.setOne(prime_power);
    ++biCode;
    if(biCode == limit)
    {
        biCode.setZero();
    }
    return *this;
}

ostream& operator<<(ostream& ostr, const GF2mElement& obj)
{
    for(int i = prime_power - 1; i >= 0; --i)
    {
        ostr << obj.biCode.get(i);
    }
    return ostr;
}

istream& operator>>(istream& istr, GF2mElement& obj)
{
    string bistr;
    istr >> bistr;
    CHECK_DEBUG( (bistr.length()==prime_power), "binary code length wrong" );
    obj.biCode.setZero();
    for(int i = 0; i < prime_power; ++i)
    {
        if(bistr[i] == '1')
        {
            obj.biCode.setOne(prime_power - 1 - i);
        }
        else if(bistr[i] != '0')
        {
            CHECK_DEBUG(false, "can not >> not binary code");
        }
    }
    return istr;
}

CUDA_HEAD bool irreducableInGF2(const BinaryCode& p)
{
    int searchUnitlPower = (p.firstNotZero() >> 1) + 1;
    BinaryCode searchLimit, searchBegin, zero, remain;
    searchLimit.setOne(searchUnitlPower);
    searchBegin.setOne(1);
    for(; searchBegin < searchLimit; ++searchBegin)
    {
        remain = (p % searchBegin);
        if(remain == zero)
        {
            return false;
        }
    }
    return true;
}

CUDA_HEAD BinaryCode findPx()
{
    BinaryCode ans;
    ans.setOne(prime_power);
    ans.setOne(0);
    ans.setOne(1);
    BinaryCode searchLimit;
    searchLimit.setOne(prime_power + 1);
    while(ans < searchLimit)
    {
        if(irreducableInGF2(ans))
        {
            return ans;
        }
        else
        {
            ++ans;//we want must have 1; so here has 2 ++ ans;
            ++ans;
        }
    }
    //for finite field theory, should not find none P(x)
    //CHECK_DEBUG(false, "should not arrival here");
    return ans;
}

void testMult()
{
    cout << "test Multiply" << endl;
    GF2mElement e1, e2, toPrint;
    int iter = (1 << prime_power);
    for(int i = 0; i < iter; ++i)
    {
        for(int j = 0; j < iter; ++j)
        {
            toPrint = e1 * e2;
            cout << toPrint << "\t";
            ++e2;
        }
        cout << endl;
        ++e1;
    }
}
void testInverse()
{
    cout << "test Inverse" << endl;
    GF2mElement e;
    int iter = (1 << prime_power) - 1;
    ++e;
    for(int i = 0; i < iter; ++i)
    {
        cout << e << " " << (!e) << " " << ((e * (!e))) << endl;
        ++e;
    }
}
void testGF2mElement()
{
    testMult();
    testInverse();
}
