#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <sstream>
#include "construct_MDS.h"
#include "predefine.h"
#include "combination.h"

using namespace std;

PruningSearch::PruningSearch()
{
    one.setOne(0);
    terminated = false;
    fout.open("Prunin_Searching_MDS.txt");

    count_to_save = COUNT_TO_SAVE;
    save_counter = 0;

    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            toCheck.setValue(i, j, one);
        }
    }
    setOrder();

}

PruningSearch::~PruningSearch()
{
    if(fout.is_open())
    {
        fout.close();
    }

    if(stateStore.is_open())
    {
        stateStore.close();
    }
}

void PruningSearch::foutOrder()
{
    fout << "Orders" << endl;
    for(int i = 0; i < MATRIX_N * MATRIX_N; ++i)
    {
        fout << rowsOrder[i] << " " << colsOrder[i] << endl;
    }

}
void PruningSearch::setOrder()
{
    /*
    int count = 0;
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            rowsOrder[count] = i;
            colsOrder[count] = j;
            ++count;
        }
    }
    */
     //another method order
    int count = 0;
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < i; ++j)
        {
            rowsOrder[count] = j;
            colsOrder[count] = i;
            ++count;
        }
        for(int j = 0; j <= i; ++j)
        {
            rowsOrder[count] = i;
            colsOrder[count] = j;
            ++count;
        }
    }

}

bool PruningSearch::checkPosition(int row, int col, int& lastProblemRow, int& lastProblemCol)
{
    save_counter = (save_counter + 1) % count_to_save;
    if(save_counter == 0)
    {
        saveState();
    }

    if(toCheck.matrix[row][col] == zero)
    {
        lastProblemRow = row;
        lastProblemCol = col;
        return false;
    }
    if(row == 0 || col == 0)
    {
        lastProblemRow = lastProblemCol = -1;
        return true;
    }

    int cm = std::min(row, col);
    //for(int m = cm; m >= 1; m -= 2)
    for(int m = 1; m <= cm; ++m)
    {
        Combination c1(row, m), c2(col, m);
        int t1 = c1.total();
        int t2 = c2.total();

        for(int i = 0; i < t1; ++i)
        {
            for(int j = 0; j < t2; ++j)
            {
                c1.comb[m] = row;
                c2.comb[m] = col;
                GF2mMatrix inverse;
                //GF2mElement determinant = toCheck.determinantRowColAndInverse(c1.comb, c2.comb, inverse, m + 1);
                GF2mElement determinant = toCheck.determinantRowCol(c1.comb, c2.comb, m + 1);
                if(determinant == zero)
                {
                    lastProblemRow = c1.comb[m - 1];
                    lastProblemCol = c2.comb[m - 1];
                    return false;
                }
                /*
                for(int irow = 0; irow <= m; ++irow)
                {
                    for(int icol = 0; icol <= m; ++ icol)
                    {
                        if(inverse.matrix[irow][icol] == zero)
                        {
                            lastProblemRow = (irow == m - 1 ? c1.comb[m - 2]: c1.comb[m - 1]);
                            lastProblemCol = (icol == m - 1 ? c2.comb[m - 2]: c1.comb[m - 1]);//may be wrong?
                            return false;
                        }
                    }
                }*/

                c2.next();
            }
            c1.next();
        }
    }
    lastProblemRow = lastProblemCol = -1;
    return true;
}

bool PruningSearch::findNext(int positionOrder, int& returnLayer)
{
    if( positionOrder >= MATRIX_N * MATRIX_N)
    {
        return true;
    }

    int row = rowsOrder[positionOrder];
    int col = colsOrder[positionOrder];
    printf("row, col = %d, %d\n", row, col);

    if(positionOrder == 0) //now our order change the last at first;
    {
        ++toCheck.matrix[MATRIX_N - 1][MATRIX_N - 1];
    }

    int problemRow = -1, problemCol = -1;

    while(toCheck.matrix[row][col] != zero)
    {
        //cout << row << " " << col << " " << toCheck.matrix[row][col] << endl;
        int lastProblemRow, lastProblemCol;


        #ifdef USE_GPU
            bool check = checkPositionGPU(row, col, lastProblemRow, lastProblemCol);
        #else
            bool check = checkPosition(row, col, lastProblemRow, lastProblemCol);
        #endif


        if(check)
        {
            int whetherReturn = 0;
            if(findNext(positionOrder + 1, whetherReturn))
            {
                return true;
            }
            else
            {

                if(whetherReturn > 0)
                {
                    toCheck.matrix[row][col] = one;
                    returnLayer = whetherReturn - 1;
                    return false;
                }
            }
        }
        else
        {
            problemRow = std::max(problemRow, lastProblemRow);
            problemCol = std::max(problemCol, lastProblemCol);
        }
        ++toCheck.matrix[row][col];
    } //now to Check[row][col] == zero;
    ++toCheck.matrix[row][col];


    //because our now order, we should return to the layer to change position of (row, problemCol)
    if(problemCol == -1)
    {
        returnLayer = 0;
    }
    else
    {
        int returnToPosition = 0;
        for(int r = positionOrder - 1; r >= 0; --r)
        {
            if(rowsOrder[r] == row && colsOrder[r] == problemCol)
            {
                returnToPosition = r;
                break;
            }
            else if(colsOrder[r] == col && rowsOrder[r] == problemRow)
            {
                returnToPosition = r;
                break;
            }
        }
        returnLayer = positionOrder - returnToPosition - 1;
        //returnLayer = 0;
        printf("return Layer %d %d, %d %d\n", row, col, problemCol, returnLayer);
    }

    if(positionOrder == 0)
    {
        terminated = true;
    }
    return false;
}

int PruningSearch::resumeSearching()
{
    loadState();
    return searching();
}

int PruningSearch::searchAtRandomBeginning()
{
    toCheck.random();

    fout << "begin at " << endl;
    fout << toCheck << endl;
    int numFromRandToEnd = searching();
    if(just_find_one && numFromRandToEnd > 0)
    {
        return numFromRandToEnd;
    }
    else
    {
        fout << "find no result from the random, let's from beginning" << endl;
        for(int i = 0; i < MATRIX_N; ++i)
        {
            for(int j = 0; j < MATRIX_N; ++j)
            {
                toCheck.setValue(i, j, one);
            }
        }
        return searching();
    }
}

int PruningSearch::searching()
{
    bool hasFound = false;

    fout << "predefine.h parameters: " << endl;
    fout << INT_SIZE << endl;
    fout << prime_base << endl;
    fout << prime_power << endl;
    fout << POLY_LIST_MAX << endl;
    fout << MATRIX_N << endl;
    fout << just_find_one << endl;

    fout << Px << endl;

    foutOrder();

    fout << endl << "record MDS matrices found" << endl;

    int foundCount = 0;
    if(just_find_one)
    {
        int useless = 0;
        hasFound = findNext(0, useless);
        if(hasFound)
        {
            fout << toCheck << endl;
            cout << "found" << endl;
            return 1;
        }
        return 0;
    }
    else
    {

        while(!terminated)
        {
            int useless = 0;
            if(findNext(0, useless))
            {
                hasFound = true;
                fout << toCheck << endl;
                ++foundCount;
            }
        }
        fout << "Totally " << foundCount << " MDS matrices" << endl;
        cout << "Totally " << foundCount << " MDS matrices" << endl;
    }
    return foundCount;
}


int PruningSearch::theoryTotalMDS()
{
    int mOne = ((1 << prime_power) - 1);
    int mTwo = mOne - 1;

    int power = 0;
    for(int i = 2; i <= MATRIX_N; ++i)
    {
        Combination c(MATRIX_N, i);
        int t = c.total();
        power = power + t * t;
    }
    double firstTerm = pow( (double)mOne, (double) (MATRIX_N * MATRIX_N - power));
    double secondTerm = pow( (double)mTwo, (double) power);
    return (int) (firstTerm * secondTerm);
}
void PruningSearch::loadState()
{
    ifstream stateLoad("state_save.txt");
    //TODO

    stateLoad >> toCheck;
}
void PruningSearch::saveState()
{
    cout << "saving state ..." << endl;
    if(stateStore.is_open())
    {
        stateStore.close();
    }
    stateStore.open("state_save.txt"); //reopen file

    stateStore << toCheck << endl;

    cout << "saving end" << endl;
}

CombineSearch::CombineSearch()
{
    one.setOne(0);
    //fout.open("Combine_Searching_MDS.txt");

    count_to_save = COUNT_TO_SAVE;
    save_counter = 0;
}

CombineSearch::~CombineSearch()
{
    if(fout.is_open())
    {
        fout.close();
    }

    if(stateStore.is_open())
    {
        stateStore.close();
    }
}

bool CombineSearch::checkPosition(int row, int col)
{
    save_counter = (save_counter + 1) % count_to_save;
    if(save_counter == 0)
    {
        saveState();
    }

    if(toCheck.matrix[row][col] == zero)
    {
        return false;
    }
    if(row == 0 || col == 0)
    {
        return true;
    }

    int cm = std::min(row, col);
    //for(int m = cm; m >= 1; m -= 2)
    for(int m = 1; m <= cm; ++m)
    {
        Combination c1(row, m), c2(col, m);
        int t1 = c1.total();
        int t2 = c2.total();

        for(int i = 0; i < t1; ++i)
        {
            for(int j = 0; j < t2; ++j)
            {
                c1.comb[m] = row;
                c2.comb[m] = col;
                GF2mMatrix inverse;
                //GF2mElement determinant = toCheck.determinantRowColAndInverse(c1.comb, c2.comb, inverse, m + 1);
                GF2mElement determinant = toCheck.determinantRowCol(c1.comb, c2.comb, m + 1);
                if(determinant == zero)
                {
                    return false;
                }
                /*
                for(int irow = 0; irow <= m; ++irow)
                {
                    for(int icol = 0; icol <= m; ++ icol)
                    {
                        if(inverse.matrix[irow][icol] == zero)
                        {
                            lastProblemRow = (irow == m - 1 ? c1.comb[m - 2]: c1.comb[m - 1]);
                            lastProblemCol = (icol == m - 1 ? c2.comb[m - 2]: c1.comb[m - 1]);//may be wrong?
                            return false;
                        }
                    }
                }*/

                c2.next();
            }
            c1.next();
        }
    }
    return true;
}

int CombineSearch::searching()
{

    int totalLastN = 0;
    for(int n = 1; n <= MATRIX_N; n <<= 1)
    {
        totalLastN = searching(n, totalLastN);
    }
    if(fout.is_open())
    {
        fout.close();
    }
    fout.open("Combine_Search_MDS_parameters.txt");
    fout << "predefine.h parameters: " << endl;
    fout << INT_SIZE << endl;
    fout << prime_base << endl;
    fout << prime_power << endl;
    fout << POLY_LIST_MAX << endl;
    fout << MATRIX_N << endl;
    fout << just_find_one << endl;

    fout << Px << endl;

    return totalLastN;
}

int CombineSearch::searching(int n, int totalLastN)
{
    //to reduce code, I implemented this just for test .. this method isn't so good...
    if(fout.is_open())
    {
        fout.close();
    }
    stringstream ss;
    ss << "CombineSearch_n=" << n << ".txt";
    fout.open(ss.str().c_str());
    /*
    bool hasFound = false;

    fout << "predefine.h parameters: " << endl;
    fout << INT_SIZE << endl;
    fout << prime_base << endl;
    fout << prime_power << endl;
    fout << POLY_LIST_MAX << endl;
    fout << MATRIX_N << endl;
    fout << just_find_one << endl;

    fout << Px << endl;

    fout << endl << "record MDS matrices found" << endl;
    */
    int foundCount = 0;

    if(n == 1)
    {
        GF2mElement e;
        ++e;
        while(e != zero)
        {
            fout << e << endl;
            ++foundCount;
            ++e;
        }
    }
    else
    {
        int lastN = (n >> 1);
        ifstream fin[4];
        stringstream sss;
        sss << "CombineSearch_n=" << lastN << ".txt";

        if(fin[0].is_open())
        {
            fin[0].close();
        }
        fin[0].open(sss.str().c_str());
        for(int i1 = 0; i1 < totalLastN; ++i1)
        {
            inputOffset(0, lastN, fin[0]);
            if(fin[1].is_open())
            {
                fin[1].close();
            }
            fin[1].open(sss.str().c_str());
            for(int i2 = 0; i2 < totalLastN; ++i2)
            {
                inputOffset(1, lastN, fin[1]);
                if(checkOffset(1, lastN))
                {
                    if(fin[2].is_open())
                    {
                        fin[2].close();
                    }
                    fin[2].open(sss.str().c_str());
                    for(int i3 = 0; i3 < totalLastN; ++i3)
                    {
                        inputOffset(2, lastN, fin[2]);
                        if(checkOffset(2, lastN))
                        {
                            if(fin[3].is_open())
                            {
                                fin[3].close();
                            }
                            fin[3].open(sss.str().c_str());
                            for(int i4 = 0; i4 < totalLastN; ++i4)
                            {
                                inputOffset(3, lastN, fin[3]);
                                if(checkOffset(3, lastN))
                                {
                                    ++foundCount;
                                    outputOffset(n, fout);
                                    if(just_find_one)
                                    {
                                        fout.close();
                                        return 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    cout << "n=" << n << " Totally " << foundCount << " MDS matrices" << endl;
    fout.close();
    return foundCount;
}

bool CombineSearch::checkOffset(int offset, int lastN)
{
    int rowOffset = (offset / 2) * lastN;
    int colOffset = (offset % 2) * lastN;

    for(int i = 0; i < lastN; ++i)
    {
        for(int j = 0; j < lastN; ++j)
        {
            if(! checkPosition(rowOffset + i, colOffset + j))
            {
                return false;
            }
        }
    }
    return true;
}
void CombineSearch::inputOffset(int offset, int lastN, ifstream& fin)
{
    int rowOffset = (offset / 2) * lastN;
    int colOffset = (offset % 2) * lastN;
    for(int i = 0; i < lastN; ++i)
    {
        for(int j = 0; j < lastN; ++j)
        {
            fin >> toCheck.matrix[rowOffset + i][colOffset + j];
        }
    }
}
void CombineSearch::outputOffset(int n, ofstream& fout)
{
    for(int i = 0; i < n; ++i)
    {
        for(int j = 0; j < n; ++j)
        {
            fout << toCheck.matrix[i][j] << "\t";
        }
        fout << endl;
    }
    fout << endl;
}
void CombineSearch::loadState()
{

    //TODO

}
void CombineSearch::saveState()
{
    //TODO
}


void testSearchMDS()
{
    PruningSearch interface;
    int experimentCount = interface.searching();
    int theoryCount = interface.theoryTotalMDS();
    cout << "experiment count: " << experimentCount << " theory count: " << theoryCount << endl;
}

void testSaveLoad()
{
    PruningSearch interface;
    const int testTimes = 3;
    for(int i = 0; i < testTimes; ++i)
    {
        interface.toCheck.random();
        cout << (interface.toCheck) << endl;
        interface.saveState();
    }
    PruningSearch restore;
    restore.loadState();
    cout << (restore.toCheck) << endl;
    cout << (interface.toCheck == restore.toCheck) << endl;
}
