#ifndef BINARY_CODE_H_INCLUDED
#define BINARY_CODE_H_INCLUDED

#include <iostream>
#include "predefine.h"

using namespace std;
/*
* It's a class for high precision binary codes
* operations:
*   ^: XOR
*   *: multiply, add is XOR
*   <<: left shift
*   ++ next element in binary code
*   /: Eucildean Division, subtraction is XOR operation
*   %: Eucildean Division's remain, subtraction is XOR operation
* Huihuang Zheng, April, 7, 2015
*/

class BinaryCode
{
public:
    int biCode[POLY_LIST_MAX];

    CUDA_HEAD BinaryCode();
    CUDA_HEAD BinaryCode(const BinaryCode& obj);
    CUDA_HEAD BinaryCode(const int inputList[]);
    CUDA_HEAD ~BinaryCode(){}

    CUDA_HEAD void setZero();
    CUDA_HEAD void setOne(int pos);
    CUDA_HEAD void setZero(int pos);
    CUDA_HEAD int get(int pos) const; //return 0 or 1 in the position

    CUDA_HEAD int firstNotZero() const;
    CUDA_HEAD void EucildeanDivision(const BinaryCode& div, BinaryCode &ans, BinaryCode &rem) const;


    CUDA_HEAD BinaryCode operator^(const BinaryCode& obj) const;
    CUDA_HEAD BinaryCode operator/(const BinaryCode& obj) const;
    CUDA_HEAD BinaryCode operator%(const BinaryCode& obj) const;
    CUDA_HEAD BinaryCode operator*(const BinaryCode& obj) const;
    CUDA_HEAD BinaryCode operator<<(int shift_left) const;
    CUDA_HEAD BinaryCode& operator++();

    CUDA_HEAD BinaryCode& operator=(const BinaryCode& obj);
    CUDA_HEAD bool operator<(const BinaryCode& obj) const;
    CUDA_HEAD bool operator==(const BinaryCode& obj) const;
    CUDA_HEAD bool operator!=(const BinaryCode& obj) const;
    friend ostream& operator<<(ostream& ostr, const BinaryCode& obj);
};

void testShiftList();
void testXOR();
void testOthers();
void testMultiply();
void testEucildeanDivision();

void testBinaryCode();


#endif // BINARY_CODE_H_INCLUDED
