#include "construct_MDS.h"
#include "predefine_GPU.h"
#include "GF2m_matrix.h"
#include "combination.h"


__global__ void gpuCheck(const int numMetrices, GF2mMatrix* toCheck, Combination* c1, Combination* c2, const int *mtxSizes,
                    int *lastProblemRows, int *lastProblemCols, bool* cudaRet)
{
    GF2mElement zero;

    CUDA_KERNEL_LOOP(i, numMetrices) {
        GF2mElement determinant = toCheck-> determinantRowCol(c1[i].comb, c2[i].comb, mtxSizes[i]);
        if(determinant == zero) {
            lastProblemRows[i] = c1[i].comb[mtxSizes[i] - 2];
            lastProblemCols[i] = c2[i].comb[mtxSizes[i] - 2];
            cudaRet[i] = false;
        }
        else {
            cudaRet[i] = true;
        }
    }
}


bool PruningSearch::checkPositionGPU(int row, int col, int& lastProblemRow, int& lastProblemCol) {

    save_counter = (save_counter + 1) % count_to_save;
    if(save_counter == 0)
    {
        saveState();
    }

    if(toCheck.matrix[row][col] == zero)
    {
        lastProblemRow = row;
        lastProblemCol = col;
        return false;
    }
    if(row == 0 || col == 0)
    {
        lastProblemRow = lastProblemCol = -1;
        return true;
    }

    int cm = std::min(row, col);

    int numMetrices = 0;

    for(int m = 1; m <= cm; ++m)
    {
        Combination c1(row, m), c2(col, m);
        int t1 = c1.total();
        int t2 = c2.total();
        numMetrices += (t1 * t2);
    }
    GF2mMatrix *deviceMatrix;
    CHECK(cudaSuccess == cudaMalloc((void**)&deviceMatrix, sizeof(GF2mMatrix)));
    CHECK(cudaSuccess == cudaMemcpy(deviceMatrix, &toCheck, sizeof(GF2mMatrix), cudaMemcpyHostToDevice));

    Combination *deviceComb1, *deviceComb2;
    CHECK(cudaSuccess == cudaMalloc((void**)&deviceComb1, numMetrices * sizeof(Combination)));
    CHECK(cudaSuccess == cudaMalloc((void**)&deviceComb2, numMetrices * sizeof(Combination)));

    int *deviceMtxSizes;
    CHECK(cudaSuccess == cudaMalloc((void**)&deviceMtxSizes, numMetrices * sizeof(int)));

    int *dLastProblemRows, *dLastProblemCols;
    CHECK(cudaSuccess == cudaMalloc((void**)&dLastProblemRows, numMetrices * sizeof(int)));
    CHECK(cudaSuccess == cudaMalloc((void**)&dLastProblemCols, numMetrices * sizeof(int)));

    bool *cudaRet;
    CHECK(cudaSuccess == cudaMalloc((void**)&cudaRet, numMetrices * sizeof(bool)));


    int iCount = 0;
    for(int m = 1; m <= cm; ++m)
    {
        Combination c1(row, m), c2(col, m);
        int t1 = c1.total();
        int t2 = c2.total();
        int matrixSize = m + 1;

        for(int i = 0; i < t1; ++i)
        {
            for(int j = 0; j < t2; ++j)
            {
                c1.comb[m] = row;
                c2.comb[m] = col;

                CHECK(cudaSuccess == cudaMemcpy(deviceComb1 + iCount, &c1, sizeof(Combination), cudaMemcpyHostToDevice));
                CHECK(cudaSuccess == cudaMemcpy(deviceComb2 + iCount, &c2, sizeof(Combination), cudaMemcpyHostToDevice));
                CHECK(cudaSuccess == cudaMemcpy(deviceMtxSizes + iCount, &matrixSize, sizeof(int), cudaMemcpyHostToDevice));

                ++iCount;
                c2.next();
            }
            c1.next();
        }
    }

    gpuCheck<<< GET_BLOCKS(numMetrices), CUDA_NUM_THREADS >>>
        (numMetrices, deviceMatrix, deviceComb1, deviceComb2, deviceMtxSizes,
        dLastProblemRows, dLastProblemCols, cudaRet);



    bool *hostRet = new bool[numMetrices];
    CHECK(cudaSuccess == cudaMemcpy(hostRet, cudaRet, numMetrices * sizeof(bool), cudaMemcpyDeviceToHost));


    for(int i = 0; i < numMetrices; ++i)
    {
        if( !hostRet[i] ) {
            CHECK(cudaSuccess == cudaMemcpy(&lastProblemRow, dLastProblemRows + i, sizeof(int), cudaMemcpyDeviceToHost));
            CHECK(cudaSuccess == cudaMemcpy(&lastProblemCol, dLastProblemCols + i, sizeof(int), cudaMemcpyDeviceToHost));

            cudaFree(deviceMatrix);
            cudaFree(deviceComb1);
            cudaFree(deviceComb2);
            cudaFree(deviceMtxSizes);
            cudaFree(dLastProblemRows);
            cudaFree(dLastProblemCols);
            cudaFree(cudaRet);
            delete[] hostRet;

            return false;
        }
    }

    cudaFree(deviceMatrix);
    cudaFree(deviceComb1);
    cudaFree(deviceComb2);
    cudaFree(deviceMtxSizes);
    cudaFree(dLastProblemRows);
    cudaFree(dLastProblemCols);
    cudaFree(cudaRet);
    delete[] hostRet;

    lastProblemRow = lastProblemCol = -1;
    return true;
}

