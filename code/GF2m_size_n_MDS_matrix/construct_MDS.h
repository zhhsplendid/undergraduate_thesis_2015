#ifndef CONSTRUCT_MDS_H_INCLUDED
#define CONSTRUCT_MDS_H_INCLUDED

#include <iostream>
#include <fstream>

#include "predefine.h"
#include "GF2m_element.h"
#include "GF2m_matrix.h"

using namespace std;

class PruningSearch
{
public:

    PruningSearch();
    ~PruningSearch();

    bool checkPosition(int row, int col, int& lastProblemRow, int& lastProblemCol);
    bool checkPositionGPU(int row, int col, int& lastProblemRow, int& lastProblemCol);

    bool findNext(int positionOrder, int& returnLayer);
    int searching(); //return number of MDS matrices has found
    int resumeSearching();
    int searchAtRandomBeginning(); //warning: this method is only used for find_just_one = true;

    void foutOrder();
    void setOrder(); //set search order

    /*
    * Reference by a paper;
    */
    int theoryTotalMDS();

    //Two functions for terminating and restoring search
    void loadState();
    void saveState();

    GF2mMatrix toCheck;

private:

    int count_to_save;
    int save_counter;
    ofstream fout, stateStore;
    bool terminated;
    GF2mElement zero, one;
    int rowsOrder[MATRIX_N * MATRIX_N], colsOrder[MATRIX_N * MATRIX_N];
};

class CombineSearch
{
    //this class is only for m must be power of 2
public:
    CombineSearch();
    ~CombineSearch();

    bool checkPosition(int row, int col);
    int searching(); //return number of MDS matrices has found
    int searching(int m, int totalLastM); //m must be power of 2;


    void loadState();
    void saveState();

    GF2mMatrix toCheck;
private:

    bool checkOffset(int offset, int lastN);
    void inputOffset(int offset, int lastN, ifstream& fin);
    void outputOffset(int n, ofstream& fout);

    int count_to_save;
    int save_counter;
    ofstream fout, stateStore;
    GF2mElement zero, one;
};

void testSearchMDS();
void testSaveLoad();
#endif // CONSTRUCT_MDS_H_INCLUDED
