#ifndef GF_ELEMENT_H_INCLUDED
#define GF_ELEMENT_H_INCLUDED

#include "predefine.h"
/*
This class is the implementation of elements in GF(p^m)
including the operation and primitive elements of GF(p^m)
Although this Class can be used for p is an any prime, but for my undergraduate thesis, I just set p = 2

The expression of GF(p^m) is the the polynomials over GF(p) whose degree is strictly less than m. Since it
GF(p^m) = GF(p)[x] / P(x) where P is an irreducible polynomial P in GF(p)[X] of degree m. We choose irreducible
X^n+aX+b as the P(x). For some fields, for example p = 2, if the polynomial X^n + X + 1 is reducible,
we choose X^n + X^k + 1 with the lowest possible k that makes the polynomial irreducible
reference -- wiki: http://en.wikipedia.org/wiki/Finite_field#Non-prime_field

The elements was
there are operations:
  + add in GF(p^m)
  * multiply in GF(p^m)
  ! the inverse of the element
  == whether one element equals another
  = give value to another;
  ++ return next element in GF;
And we should implement the operator function:
  << to see the element;

Huihuang Zheng
April 5, 2015
*/

//TODO because we just use 2^m, we can accelerate the element. see GF2mElement.h
class Element
{
public:
    /*
    * An empty construction
    */
    CUDA_HEAD Element(){};

    /*
    * delete the polynomial list
    */
    CUDA_HEAD ~Element(){};

protected:

    int polyList[POLY_LIST_MAX];
};


#endif // GF2M_ELEMENT_H_INCLUDED
