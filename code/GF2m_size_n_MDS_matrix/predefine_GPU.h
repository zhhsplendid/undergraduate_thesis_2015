#ifndef CHECKPOSITIONGPU_H_INCLUDED
#define CHECKPOSITIONGPU_H_INCLUDED

// CUDA: use 512 threads per block
const int CUDA_NUM_THREADS = 512;

// CUDA: number of blocks for threads.
inline int GET_BLOCKS(const int N) {
  return (N + CUDA_NUM_THREADS - 1) / CUDA_NUM_THREADS;
}

#define CUDA_KERNEL_LOOP(i, n) \
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; \
       i < (n); \
       i += blockDim.x * gridDim.x)


template <typename Dtype>
void showArrayGPU(const int N, const Dtype* arr);


#endif // CHECKPOSITIONGPU_H_INCLUDED
