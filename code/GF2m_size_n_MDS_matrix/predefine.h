#ifndef PREDEFINE_H_INCLUDED
#define PREDEFINE_H_INCLUDED

#include<cstdio>
#include<cstdlib>

#define DEBUG true //comment it will let CHECK useless

//#define USE_GPU//comment it to use GPU or not


#if DEBUG
    #define CHECK(condition) \
    if(!condition)\
    {\
        printf("Check fail at %s line %d\n", __FILE__, __LINE__);\
        exit(1);\
    }
    #define CHECK_DEBUG(condition,value) \
    if(!condition)\
    {\
        cout << __FILE__ << " line " << __LINE__ << " " << value << endl;\
        exit(1);\
    }
    #define DEBUG_VALUE(value) cout << value << endl;
#else
    #define CHECK(condition)
    #define CHECK_DEBUG(condition,value)
    #define DEBUG_VALUE(value)
#endif // DEBUG

#ifdef USE_GPU
#define CUDA_HEAD __host__ __device__
#else
#define CUDA_HEAD
#endif
const int POLY_LIST_MAX= 8; // don't change unless you know the code
const int INT_SIZE = 16; // don't change unless you know the code

const int prime_base = 2; // don't change. Until now we suppurt 2^m finite field. If you extend our program to p^m finite field, then you can change it.

const int prime_power = 2; // you can change here! The m value for 2^m finite field. Also the m value in paper.
const int MATRIX_N = 2; // you can change here for matrix size. Also the n value in paper.

const int COUNT_TO_SAVE = 10000; //how many times when we create a snapshot for saving our searching state (so that we can close program and next time we can resume searching)

const bool just_find_one = true; //just find one MDS matrix then exit algorithm or you want to find a lot.

const bool resume_search = false; //set true if you have a saved state file and resume searching so that you don't need to start from begining
const bool random_begin = false; //set true if you don't search from begining but a random searching state.
//Notice: random begin is just call when search from begining, it has no effect if you resume searching.

#endif // PREDEFINE_H_INCLUDED


