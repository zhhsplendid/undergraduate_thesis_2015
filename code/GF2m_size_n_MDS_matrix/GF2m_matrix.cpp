#include <iostream>
#include <cstdio>
#include "GF2m_matrix.h"

using namespace std;

CUDA_HEAD GF2mMatrix::GF2mMatrix(const GF2mMatrix& m)
{
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            matrix[i][j] = m.matrix[i][j];
        }
    }
}

void GF2mMatrix::random()
{
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            matrix[i][j].random();
        }
    }
}

CUDA_HEAD void GF2mMatrix::swapRowSizeN(const int row1, const int row2, const int begin, const int end)
{
    GF2mElement tmp;
    for(int i = begin; i < end; ++i)
    {
        tmp = matrix[row1][i];
        matrix[row1][i] = matrix[row2][i];
        matrix[row2][i] = tmp;
    }
}

CUDA_HEAD GF2mElement GF2mMatrix::determinantAndInverseSizeN(const int N, GF2mMatrix& inverse)
{
    //the input GF2mMatrix should be a zero matrix;
    GF2mElement answer, zero, one;
    answer.setOne(0);
    one.setOne(0);

    //GF2mMatrix copym = (*this);

    for(int i = 0; i < N; ++i)
    {
        inverse.matrix[i][i] = one;
    }

    //Gauss elimination
    for(int i = 0; i < N; ++i)
    {
        //cout << (*this) << endl;
        if(matrix[i][i] == zero)
        {
            //search for let matrix[i][i] != zero
            for(int j = i + 1; j < N; ++j)
            {
                if(matrix[j][i] != zero)
                {
                    swapRowSizeN(j, i, i, N);
                    inverse.swapRowSizeN(j, i, 0, N);
                }
            }
            //if still has zero, determinant = 0;
            if(matrix[i][i] == zero)
            {
                return zero; //can not be inverse
            }
        }
        //now matrix[i][i] != zero
        GF2mElement div = (!matrix[i][i]);
        for(int j = i + 1; j < N; ++j)
        {
            if(matrix[j][i] != zero)
            {
                GF2mElement mult = div * matrix[j][i];
                for(int k = i; k < N; ++k)
                {
                    matrix[j][k] = (matrix[j][k] ^ (matrix[i][k] * mult));

                }
                for(int k = 0; k < N; ++k)
                {
                    inverse.matrix[j][k] = (inverse.matrix[j][k] ^ (inverse.matrix[i][k] * mult));
                }
            }
        }
    }
    //cout << answer << endl;
    for(int i = 0; i < N; ++i)
    {
        answer = answer * matrix[i][i];
    }

    //now the matrix[][] is an upper triangle matrix
    if(answer != zero) //No zero in the diagonal
    {

        //cout << (*this) << endl;
        //cout << (inverse * copym) << endl;
        //cout << inverse << endl;
        for(int i = 0; i < N; ++i)
        {
            GF2mElement div = (!matrix[i][i]);
            for(int j = i; j < N; ++j)
            {
                 matrix[i][j] = matrix[i][j] * div;
            }
            for(int j = 0; j < N; ++j)
            {
                inverse.matrix[i][j] = inverse.matrix[i][j] * div;
            }
        }
        //cout << (*this) << endl;
        //cout << (copym * inverse) << endl;
        //cout << inverse << endl;
        for(int i = 0; i < N; ++i)
        {
            for(int j = 0; j < i; ++j)
            {
                GF2mElement mult = matrix[j][i];
                for(int k = i; k < N; ++k)
                {
                    matrix[j][k] = matrix[j][k] ^ (matrix[i][k] * mult);
                }
                for(int k = 0; k < N; ++k)
                {
                    inverse.matrix[j][k] = inverse.matrix[j][k] ^ (inverse.matrix[i][k] * mult);
                }
            }
        }
        //cout << (*this) << endl;
        //cout << (copym * inverse) << endl;
        //cout << inverse << endl;
    }

    return answer;
}

CUDA_HEAD GF2mElement GF2mMatrix::determinantSizeN(const int N)
{
    GF2mElement answer, zero;
    answer.setOne(0);

    //Gauss elimination
    for(int i = 0; i < N; ++i)
    {
        //cout << (*this) << endl;
        if(matrix[i][i] == zero)
        {
            //search for let matrix[i][i] != zero
            for(int j = i + 1; j < N; ++j)
            {
                if(matrix[j][i] != zero)
                {
                    swapRowSizeN(j, i, i, N);
                }
            }
            //if still has zero, determinant = 0;
            if(matrix[i][i] == zero)
            {
                return zero;
            }
        }
        //now matrix[i][i] != zero
        GF2mElement div = (!matrix[i][i]);
        for(int j = i + 1; j < N; ++j)
        {
            if(matrix[j][i] != zero)
            {
                GF2mElement mult = div * matrix[j][i];
                for(int k = i; k < N; ++k)
                {
                    matrix[j][k] = (matrix[j][k] ^ (matrix[i][k] * mult));
                }
            }
        }
    }
    //cout << answer << endl;
    for(int i = 0; i < N; ++i)
    {
        answer = answer * matrix[i][i];
    }
    return answer;
}

CUDA_HEAD GF2mElement GF2mMatrix::determinantRowCol(const int rows[], const int cols[], const int N) const
{
    GF2mMatrix toCompute;
    for(int i = 0; i < N; ++i)
    {
        for(int j = 0; j < N; ++j)
        {
            toCompute.setValue(i, j, matrix[rows[i]][cols[j]]);
        }
    }
    return toCompute.determinantSizeN(N);
}

CUDA_HEAD GF2mElement GF2mMatrix::determinantRowColAndInverse(const int rows[], const int cols[],
    GF2mMatrix& inverse, const int N) const
{
    GF2mMatrix toCompute;
    for(int i = 0; i < N; ++i)
    {
        for(int j = 0; j < N; ++j)
        {
            toCompute.setValue(i, j, matrix[rows[i]][cols[j]]);
        }
    }
    return toCompute.determinantAndInverseSizeN(N, inverse);
}

CUDA_HEAD void GF2mMatrix::setValue(int row, int col, const GF2mElement& elem)
{
    matrix[row][col] = elem;
}

CUDA_HEAD GF2mMatrix GF2mMatrix::operator*(const GF2mMatrix& m) const
{
    GF2mMatrix answer;
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            for(int k = 0; k < MATRIX_N; ++k)
            {
                answer.matrix[i][j] = answer.matrix[i][j] ^ matrix[i][k] * m.matrix[k][j];
            }
        }
    }
    return answer;
}

CUDA_HEAD GF2mMatrix& GF2mMatrix::operator=(const GF2mMatrix& m)
{
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            matrix[i][j] = m.matrix[i][j];
        }
    }
    return (*this);
}

CUDA_HEAD bool GF2mMatrix::operator==(const GF2mMatrix& m) const
{
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            if(matrix[i][j] != m.matrix[i][j])
            {
                return false;
            }
        }
    }
    return true;
}
ostream& operator<<(ostream& ostr, const GF2mMatrix& m)
{
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            ostr << m.matrix[i][j] << '\t';
        }
        ostr << endl;
    }
    return ostr;
}

istream& operator>>(istream& istr, GF2mMatrix& m)
{
    for(int i = 0; i < MATRIX_N; ++i)
    {
        for(int j = 0; j < MATRIX_N; ++j)
        {
            istr >> m.matrix[i][j];
        }
    }
    return istr;
}

void testDeterminant()
{
    cout << "test determinant for MATRIX_N = 4" << endl;
    GF2mMatrix m;
    GF2mElement elem;

    for(int i = 0; i < MATRIX_N * MATRIX_N; ++i)
    {
        ++elem;
        m.setValue(i / MATRIX_N, i % MATRIX_N, elem);
    }

    cout << m.determinantSizeN(MATRIX_N);
}

void testInverseMatrix()
{
    cout << "test inverse matrix for MATRIX_N = 4" << endl;
    GF2mMatrix m, inverse;
    GF2mElement elem, zero;

    m.random();
    //cout << m << endl;
    GF2mMatrix copym = m;
    GF2mElement dete = m.determinantAndInverseSizeN(MATRIX_N, inverse);
    cout << dete << endl;

    //cout << inverse << endl;
    if(dete != zero)
    {
        cout << (copym * inverse) << endl;
    }
}
