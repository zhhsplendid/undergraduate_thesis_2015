#include <iostream>

#include "predefine_GPU.h"

template <typename Dtype>
void showArrayGPU(const int N, const Dtype* arr)
{
    Dtype* toShow = new Dtype[N];
    cudaMemcpy(toShow, arr, N * sizeof(Dtype), cudaMemcpyDeviceToHost);
    for(int i = 0; i < N; ++i) {
        std::cout << toShow << '\t';
    }
    std::cout << std::endl;
}
