#ifndef COMBINATION_H_INCLUDED
#define COMBINATION_H_INCLUDED

#include "predefine.h"
/*
* This class returns C_n^m arrays
*/

class Combination
{
public:

    Combination(const int n, const int m);
    ~Combination(){}

    int total();
    void next();
    int comb[MATRIX_N];

    int getN() {return cn;}
    int getM() {return cm;}

private:
    const int cn;
    const int cm;
};

void testCombination();
#endif // COMBINATION_H_INCLUDED
