#include "predefine.h"
#include "math_functions.h"

int BiListToNum(const int N, const int inputList[])
{
    int result = 0;
    for(int i = 0; i < N; ++i)
    {
        CHECK(inputList[i] == 0 || inputList[i] == 1);
        result |= (inputList[i] << i);
    }
    return result;
}


