#ifndef GF2M_ELEMENT_H_INCLUDED
#define GF2M_ELEMENT_H_INCLUDED

#include <iostream>


#include "binary_code.h"
#include "GF_element.h"

using namespace std;
/*
This class inherits the class Element, as set prime = 2, see GF_element.h
But this class does not use a list to store the polynomial in GF, it uses binary number to
accelerate the computation.
*/
class GF2mElement: public Element
{
public:
    /*
    * default construction function, to set a element to zero in GF(p^m)
    */
    CUDA_HEAD GF2mElement();
    CUDA_HEAD GF2mElement(const BinaryCode& obj);
    CUDA_HEAD GF2mElement(const GF2mElement& elem);
    CUDA_HEAD ~GF2mElement(){};

    CUDA_HEAD void setZero();
    CUDA_HEAD void setZero(int pos);

    CUDA_HEAD void setOne(int pos);
    void random();
    /*
    * operator functions
    */
    CUDA_HEAD GF2mElement operator^(const GF2mElement& elem) const;
    CUDA_HEAD GF2mElement operator*(const GF2mElement& elem) const;
    CUDA_HEAD GF2mElement operator!() const;
    CUDA_HEAD bool operator==(const GF2mElement& elem) const;
    CUDA_HEAD bool operator!=(const GF2mElement& elem) const;
    CUDA_HEAD bool operator<(const GF2mElement& elem) const;
    CUDA_HEAD GF2mElement& operator=(const GF2mElement& elem);
    CUDA_HEAD GF2mElement& operator++();
    friend ostream& operator<<(ostream& ostr, const GF2mElement& obj);
    friend istream& operator>>(istream& istr, GF2mElement& obj);
protected:
    BinaryCode biCode;

    #ifdef USE_GPU
    BinaryCode Px;
    #endif
};

CUDA_HEAD bool irreducableInGF2(const BinaryCode& p);
CUDA_HEAD BinaryCode findPx();
static BinaryCode Px = findPx();// the P(x) for GF(2)/P(x), P(x) must have prime_power degree

void testMult();
void testInverse();
void testGF2mElement();
#endif // GF2M_ELEMENT_H_INCLUDED
