#include <string.h>
#include <iostream>
#include <iomanip>
#include "binary_code.h"

using namespace std;

CUDA_HEAD BinaryCode::BinaryCode()
{
    setZero();
}

CUDA_HEAD BinaryCode::BinaryCode(const BinaryCode& obj)
{
    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        biCode[i] = obj.biCode[i];
    }
}

CUDA_HEAD BinaryCode::BinaryCode(const int inputList[])
{
    int mask = (1 << INT_SIZE) - 1;
    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        biCode[i] = (inputList[i] & mask);
    }
}

CUDA_HEAD void BinaryCode::setZero()
{
    //memset(biCode, 0, sizeof(int) * POLY_LIST_MAX);
    for(int i = 0; i < POLY_LIST_MAX; ++i) {
        biCode[i] = 0;
    }
}

CUDA_HEAD void BinaryCode::setOne(int pos)
{
    biCode[pos/INT_SIZE] |= (1 << (pos % INT_SIZE));
}

CUDA_HEAD int BinaryCode::get(int pos) const
{
    if(biCode[pos/INT_SIZE] & (1 << (pos % INT_SIZE)))
    {
        return 1;
    }
    return 0;
}

CUDA_HEAD void BinaryCode::setZero(int pos)
{
    int mask = (1 << (pos % INT_SIZE));
    if(biCode[pos/INT_SIZE] & mask)
    {
        biCode[pos/INT_SIZE] ^= mask;
    }

}

CUDA_HEAD int BinaryCode::firstNotZero() const
{
    for(int i = POLY_LIST_MAX - 1; i >= 0; --i)
    {
        for(int j = INT_SIZE - 1; j >= 0; --j)
        {
            if(biCode[i] & (1 << j))
            {
                return i * INT_SIZE + j;
            }
        }
    }
    return -1;
}

CUDA_HEAD void BinaryCode::EucildeanDivision(const BinaryCode& div, BinaryCode &quo, BinaryCode &rem) const
{
    int div_first_one_pos = div.firstNotZero();
    //CHECK_DEBUG( (div_first_one_pos != -1), "divide 0 error");
    quo.setZero();
    rem = (*this);

    for(int i = POLY_LIST_MAX - 1; i >= 0; --i)
    {
        for(int j = INT_SIZE - 1; j >= 0; --j)
        {
            int pos = i * INT_SIZE + j;
            if(pos < div_first_one_pos)
            {
                break;
            }

            if(rem.biCode[i] & (1 << j))
            {
                int shift = pos - div_first_one_pos;
                quo.biCode[shift / INT_SIZE] |= (1 << (shift % INT_SIZE));
                rem = rem ^ (div << (shift));
            }
        }
    }
}

CUDA_HEAD BinaryCode BinaryCode::operator/(const BinaryCode& obj) const
{
    BinaryCode quotient, remain;
    EucildeanDivision(obj, quotient, remain);
    return quotient;
}

CUDA_HEAD BinaryCode BinaryCode::operator%(const BinaryCode& obj) const
{
    BinaryCode quotient, remain;
    EucildeanDivision(obj, quotient, remain);
    return remain;
}

CUDA_HEAD BinaryCode BinaryCode::operator*(const BinaryCode& obj) const
{
    BinaryCode ans;
    ans.setZero();

    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        for(int j = 0; j < INT_SIZE; ++j)
        {
            if(obj.biCode[i] & (1 << j))
            {
                ans = ans ^ ((*this) << (i * INT_SIZE + j));
            }
        }
    }
    return ans;
}

CUDA_HEAD BinaryCode BinaryCode::operator<<(int shift_left) const
{
    BinaryCode ans;
    ans.setZero();

    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        int pos = i + shift_left / INT_SIZE;
        int mask = (1 << (INT_SIZE - shift_left % INT_SIZE)) - 1;
        if(pos < POLY_LIST_MAX)
        {
            ans.biCode[pos] |= ((biCode[i] & mask) << (shift_left % INT_SIZE));
        }
        ++pos;
        mask = ((1 << INT_SIZE) - 1) ^ mask;
        if(pos < POLY_LIST_MAX)
        {
            ans.biCode[pos] |= ((biCode[i] & mask) >> (INT_SIZE - shift_left % INT_SIZE));
        }
    }
    return ans;
}

ostream& operator<<(ostream& ostr, const BinaryCode& obj)
{
    for(int i = POLY_LIST_MAX - 1; i >= 0; --i)
    {
        for(int j = INT_SIZE - 1; j >= 0; --j)
        {
            if( (obj.biCode[i] & (1 << j)) != 0)
            {
                ostr << 1;
            }
            else
            {
                ostr << 0;
            }
        }
    }
    return ostr;
}

CUDA_HEAD BinaryCode BinaryCode::operator^(const BinaryCode& obj) const
{
    BinaryCode ans;
    ans.setZero();

    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        ans.biCode[i] = biCode[i] ^ obj.biCode[i];
    }
    return ans;
}

CUDA_HEAD bool BinaryCode::operator==(const BinaryCode& obj) const
{
    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        if(biCode[i] != obj.biCode[i])
        {
            return false;
        }
    }
    return true;
}

CUDA_HEAD bool BinaryCode::operator!=(const BinaryCode& obj) const
{
    return ! ((*this) == obj);
}

CUDA_HEAD bool BinaryCode::operator<(const BinaryCode& obj) const
{
    for(int i = POLY_LIST_MAX - 1; i >= 0; --i)
    {
        if(biCode[i] < obj.biCode[i])
        {
            return true;
        }
        else if(biCode[i] > obj.biCode[i])
        {
            return false;
        }
    }
    return false;
}

CUDA_HEAD BinaryCode& BinaryCode::operator=(const BinaryCode& obj)
{
    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        biCode[i] = obj.biCode[i];
    }
    return *this;
}

CUDA_HEAD BinaryCode& BinaryCode::operator++()
{
    int base = (1 << INT_SIZE);
    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        ++biCode[i];

        if(biCode[i] < base)
        {
            break;
        }
        else
        {
            biCode[i] = 0;
        }
    }
    return *this;
}


void testShiftList()
{
    cout << "test Shift left" << endl;
    int a[POLY_LIST_MAX];
    a[0] = 1;
    BinaryCode ba(a);

    cout << ba << endl;
    cout << (ba << (INT_SIZE - 1)) << endl;
    cout << (ba << INT_SIZE) << endl;
    cout << (ba << (INT_SIZE + 1)) << endl;

    int b[POLY_LIST_MAX];
    for(int i = 0; i < POLY_LIST_MAX; ++i)
    {
        b[i] = (1 << INT_SIZE) - 1;
    }
    BinaryCode bb(b);
    cout << bb << endl;
    cout << (bb << (2 * INT_SIZE - 1)) << endl;
    cout << (bb << (2 * INT_SIZE)) << endl;
    cout << (bb << (2 * INT_SIZE + 1)) << endl;
}

void testXOR()
{
    cout << "test XOR" << endl;
    BinaryCode a1, a2;
    cout << a1 << endl;
    cout << a2 << endl;
    cout << (a1 ^ a2) << endl;
}

void testOthers()
{
    cout << "test others" << endl;
    BinaryCode b1, b2;

    b1.setOne(12);
    b1.setOne(1);
    b1.setOne(3);
    cout << b1 << endl;
    b1.setZero(3);
    b2 = b1;
    ++b2;
    cout << b2 << endl;
    cout << b2.firstNotZero() << endl;
    cout << (b2 == b2) << endl;
    cout << (b1 == b2) << endl;
}

void testMultiply()
{
    cout << "test multiply" << endl;
    int a[POLY_LIST_MAX], b[POLY_LIST_MAX];
    BinaryCode b1(a), b2(b);
    cout << b1 << endl;
    cout << b2 << endl;
    BinaryCode first = b1 * b2;
    cout << first << endl;


    ++b1;
    cout << b1 << endl;
    cout << b2 << endl;
    BinaryCode second = (b1 * b2);
    cout << second << endl;
    cout << ( (first ^ second) == b2) << endl;

    b2.setZero();
    cout << (b1 * b2) << endl;
}

void testEucildeanDivision()
{
    cout << "test Eucildean Division" << endl;
    BinaryCode b1, b2;
    for(int i = 0; i < (1 << 24) - 1; ++i)
    {
        ++b1;
    }
    for(int i = 0; i < (1 << 3) - 1; ++i)
    {
        ++b2;
    }
    cout << b1 << endl;
    cout << b2 << endl;
    BinaryCode quo = b1 / b2;
    BinaryCode rem = b1 % b2;
    cout << quo << endl;
    cout << rem << endl;

    BinaryCode toCmp = ( (quo * b2) ^ rem);
    cout << toCmp << endl;
    cout << (toCmp == b1) << endl;
}

void testBinaryCode()
{
    testShiftList();
    testXOR();
    testOthers();
    testMultiply();
    testEucildeanDivision();
}
