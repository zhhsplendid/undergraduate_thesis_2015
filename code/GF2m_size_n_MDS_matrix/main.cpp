#include <iostream>
#include <cstdlib>
#include <ctime>

#include "predefine.h"
#include "GF2m_element.h"
#include "GF2m_matrix.h"
#include "construct_MDS.h"

using namespace std;

void init()
{
    srand(time(0));
}

int main()
{
    init();
    //testSearchMDS();
    //CombineSearch interface;
    PruningSearch interface;
    if(resume_search)
    {
        interface.resumeSearching();
    }
    else if(random_begin)
    {
        interface.searchAtRandomBeginning();
    }
    else
    {
        interface.searching();
    }
    return 0;
}
