
#include<iostream>

#include "predefine.h"
#include "combination.h"

using namespace std;

Combination::Combination(const int n, const int m): cn(n), cm(m)
{
    //should check 0 <= m <= n
    for(int i = 0; i < cm; ++i)
    {
        comb[i] = i;
    }
}

int Combination::total()
{
    int ans = 1;
    for(int i = 0; i < cm; ++i)
    {
        ans = ans * (cn - i) / (i + 1);
    }
    return ans;
}

void Combination::next()
{
    for(int i = 0; i < cm - 1; ++i)
    {
        if(comb[i] + 1 < comb[i + 1])
        {
            ++comb[i];
            for(int j = 0; j < i; ++j)
            {
                comb[j] = j;
            }
            return;
        }
    }

    if(comb[cm - 1] + 1 >= cn) //all combinations over, from the beginning
    {
        for(int i = 0; i < cm; ++i)
        {
            comb[i] = i;
        }
    }
    else
    {
        ++comb[cm - 1];
        for(int j = 0; j < cm - 1; ++j)
        {
            comb[j] = j;
        }
    }
}

void testCombination()
{
    cout << "test Combinations" << endl;
    Combination cmn(6, 3);
    CHECK(MATRIX_N >= 6);
    int total = cmn.total();
    for(int i = 0; i < 2 * total; ++i)
    {
        for(int j = 0; j < cmn.getM(); ++j)
        {
            cout << cmn.comb[j] << ", ";
        }
        cout << endl;
        cmn.next();
    }
}
