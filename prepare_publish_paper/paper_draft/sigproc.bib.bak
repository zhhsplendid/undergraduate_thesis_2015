
@inproceedings{peyrin2015lightweight,
  title={Lightweight {MDS} Involution Matrices},
  author={Peyrin, Thomas},
  booktitle={Fast Software Encryption: 22nd International Workshop, FSE 2015, Istanbul, Turkey, March 8-11, 2015, Revised Selected Papers},
  volume={9054},
  pages={471},
  year={2015},
  organization={Springer}
}

@article{sajadieh2012construction,
  title={On construction of involutory {MDS} matrices from {Vandermonde} Matrices in {$GF(2^q)$}},
  author={Sajadieh, Mahdi and Dakhilalian, Mohammad and Mala, Hamid and Omoomi, Behnaz},
  journal={Designs, Codes and Cryptography},
  volume={64},
  number={3},
  pages={287--308},
  year={2012},
  publisher={Springer}
}


@incollection{gupta2013constructions,
  title={On constructions of involutory {MDS} matrices},
  author={Gupta, Kishan Chand and Ray, Indranil Ghosh},
  booktitle={Progress in Cryptology--AFRICACRYPT 2013},
  pages={43--60},
  year={2013},
  publisher={Springer}
}

@inproceedings{wu2013recursive,
  title={Recursive diffusion layers for (lightweight) block ciphers and hash functions},
  author={Wu, Shengbao and Wang, Mingsheng and Wu, Wenling},
  booktitle={Selected Areas in Cryptography},
  pages={355--371},
  year={2013},
  organization={Springer}
}

@inproceedings{augot2013exhaustive,
  title={Exhaustive search for small dimension recursive {MDS} diffusion layers for block ciphers and hash functions},
  author={Augot, Daniel and Finiasz, Matthieu},
  booktitle={Information Theory Proceedings (ISIT), 2013 IEEE International Symposium on},
  pages={1551--1555},
  year={2013},
  organization={IEEE}
}

@article{daemen1999aes,
  title={{AES} proposal: Rijndael},
  author={Daemen, Joan and Rijmen, Vincent},
  year={1999}
}

@book{schneier1999twofish,
  title={The {Twofish} encryption algorithm: a 128-bit block cipher},
  author={Schneier, Bruce and Kelsey, John and Whiting, Doug and Wagner, David and Hall, Chris and Ferguson, Niels},
  year={1999},
  publisher={John Wiley \& Sons, Inc.}
}

@inproceedings{rijmen1996cipher,
  title={The cipher {SHARK}},
  author={Rijmen, Vincent and Daemen, Joan and Preneel, Bart and Bosselaers, Antoon and De Win, Erik},
  booktitle={Fast Software Encryption},
  pages={99--111},
  year={1996},
  organization={Springer}
}

@inproceedings{augot2014direct,
  title={Direct construction of recursive {MDS} diffusion layers using shortened {BCH} codes},
  author={Augot, Daniel and Finiasz, Matthieu},
  booktitle={Fast Software Encryption},
  pages={3--17},
  year={2014},
  organization={Springer}
}

@incollection{gupta2014constructions,
  title={On constructions of circulant {MDS} matrices for lightweight cryptography},
  author={Gupta, Kishan Chand and Ray, Indranil Ghosh},
  booktitle={Information Security Practice and Experience},
  pages={564--576},
  year={2014},
  publisher={Springer}
}

@inproceedings{sajadieh2012recursive,
  title={Recursive diffusion layers for block ciphers and hash functions},
  author={Sajadieh, Mahdi and Dakhilalian, Mohammad and Mala, Hamid and Sepehrdad, Pouyan},
  booktitle={Fast Software Encryption},
  pages={385--401},
  year={2012},
  organization={Springer}
}

@incollection{guo2011photon,
  title={The {PHOTON} family of lightweight hash functions},
  author={Guo, Jian and Peyrin, Thomas and Poschmann, Axel},
  booktitle={Advances in Cryptology--CRYPTO 2011},
  pages={222--239},
  year={2011},
  publisher={Springer}
}

@incollection{shirai2004feistel,
  title={On {Feistel} ciphers using optimal diffusion mappings across multiple rounds},
  author={Shirai, Taizo and Preneel, Bart},
  booktitle={Advances in Cryptology-ASIACRYPT 2004},
  pages={1--15},
  year={2004},
  publisher={Springer}
}

@phdthesis{daemen1995cipher,
  title={Cipher and hash function design strategies based on linear and differential cryptanalysis},
  author={Daemen, Joan},
  year={1995},
  school={Doctoral Dissertation, March 1995, KU Leuven}
}

@inproceedings{berrou2002computing,
  title={Computing the minimum distance of linear codes by the error impulse method},
  author={Berrou, Claude and Vaton, Sandrine and Jezequel, Michel and Douillard, Catherine},
  booktitle={Global Telecommunications Conference, 2002. GLOBECOM'02. IEEE},
  volume={2},
  pages={1017--1020},
  year={2002},
  organization={IEEE}
}

@incollection{lacan2004construction,
  title={A construction of matrices with no singular square submatrices},
  author={Lacan, J{\'e}r{\^o}me and Fimes, J{\'e}r{\^o}me},
  booktitle={Finite Fields and Applications},
  pages={145--147},
  year={2004},
  publisher={Springer}
}

@inproceedings{shirai2004improving,
  title={Improving immunity of {Feistel} ciphers against differential cryptanalysis by using multiple {MDS} matrices},
  author={Shirai, Taizo and Shibutani, Kyoji},
  booktitle={Fast Software Encryption},
  pages={260--278},
  year={2004},
  organization={Springer}
}

@article{dumer2003hardness,
  title={Hardness of approximating the minimum distance of a linear code},
  author={Dumer, Ilya and Micciancio, Daniele and Sudan, Madhu},
  journal={Information Theory, IEEE Transactions on},
  volume={49},
  number={1},
  pages={22--37},
  year={2003},
  publisher={IEEE}
}



@inproceedings{junod2005perfect,
  title={Perfect diffusion primitives for block ciphers},
  author={Junod, Pascal and Vaudenay, Serge},
  booktitle={Selected Areas in Cryptography},
  pages={84--99},
  year={2005},
  organization={Springer}
}



@inproceedings{wu2011lblock,
  title={LBlock: a lightweight block cipher},
  author={Wu, Wenling and Zhang, Lei},
  booktitle={Applied Cryptography and Network Security},
  pages={327--344},
  year={2011},
  organization={Springer}
}

@inproceedings{suzaki2013textnormal,
  title={{TWINE}: A Lightweight Block Cipher for Multiple Platforms},
  author={Suzaki, Tomoyasu and Minematsu, Kazuhiko and Morioka, Sumio and Kobayashi, Eita},
  booktitle={Selected Areas in Cryptography},
  pages={339--354},
  year={2013},
  organization={Springer}
}

@incollection{izadi2009mibs,
  title={{MIBS}: a new lightweight block cipher},
  author={Izadi, Maryam and Sadeghiyan, Babak and Sadeghian, Seyed Saeed and Khanooki, Hossein Arabnezhad},
  booktitle={Cryptology and Network Security},
  pages={334--348},
  year={2009},
  publisher={Springer}
}

@inproceedings{matsui1994linear,
  title={Linear cryptanalysis method for {DES} cipher},
  author={Matsui, Mitsuru},
  booktitle={Advances in Cryptology��EUROCRYPT��93},
  pages={386--397},
  year={1994},
  organization={Springer}
}

@techreport{dworkin2001recommendation,
  title={Recommendation for block cipher modes of operation. methods and techniques},
  author={Dworkin, Morris},
  year={2001},
  institution={DTIC Document}
}

@article{biham1991differential,
  title={Differential cryptanalysis of {DES-like} cryptosystems},
  author={Biham, Eli and Shamir, Adi},
  journal={Journal of CRYPTOLOGY},
  volume={4},
  number={1},
  pages={3--72},
  year={1991},
  publisher={Springer}
}

@article{burr2003selecting,
  title={Selecting the advanced encryption standard},
  author={Burr, William E},
  journal={IEEE Security \& Privacy},
  number={2},
  pages={43--52},
  year={2003},
  publisher={IEEE}
}

@inproceedings{youssef1997design,
  title={On the design of linear transformations for substitution permutation encryption networks},
  author={Youssef, AM and Mister, S and Tavares, SE},
  booktitle={Workshop on Selected Areas of Cryptography (SAC��96): Workshop Record},
  pages={40--48},
  year={1997}
}

@book{macwilliams1977theory,
  title={The theory of error correcting codes},
  author={MacWilliams, Florence Jessie and Sloane, Neil James Alexander},
  year={1977},
  publisher={Elsevier}
}

@misc{daemen2002design,
  title={The Design of Rijndael. Secaucus},
  author={Daemen, Joan and Rijmen, Vincent},
  year={2002},
  publisher={NJ, USA: Springer-Verlag New York, Inc}
}
